package com.example.assistclient;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.alicecallsbob.assist.sdk.config.AssistConfig;
import com.alicecallsbob.assist.sdk.config.impl.AssistCobrowseListener;
import com.alicecallsbob.assist.sdk.config.impl.AssistConfigBuilder;
import com.alicecallsbob.assist.sdk.core.Assist;
import com.alicecallsbob.assist.sdk.core.AssistError;
import com.alicecallsbob.assist.sdk.core.AssistListener;

public class MainActivity extends AppCompatActivity implements AssistListener, AssistCobrowseListener {

    // we will need a reference to the 'Help!' button, since we will enable
    // and disable it according to whether we are on a support session or not
    private Button helpButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        // this class will act as both AssistListener,
        // and AssistCobrowseListener.
        final AssistListener assistListener = this;
        final AssistCobrowseListener cobrowseListener = this;

        // wire up the 'Help!' button click handler
        this.helpButton = (Button) this.findViewById(R.id.help);
        this.helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // while the Assist object will gracefully handle additional
                // invocations of the startSupport method, we will cause this button
                // to become un-touchable while the support session is active
                helpButton.setEnabled(false);

                // build an AssistConfig using the AssistConfigBuilder
                AssistConfigBuilder builder = new AssistConfigBuilder(getApplicationContext())
                        .setServerHost("rp.example.com")
                        .setServerPort(8080)
                        .setCobrowseListener(cobrowseListener)
                        .setAgentName("agent1");

                // build the config object
                AssistConfig config = builder.build();

                // start the support session
                Assist.startSupport(config, getApplication(), assistListener);
            }
        });
    }


    ///////////////////////////////
    //
    // AssistListener methods

    @Override
    public void onSupportEnded(boolean b) {
        this.helpButton.setEnabled(true);
    }

    @Override
    public void onSupportError(AssistError assistError, String s) {
        this.helpButton.setEnabled(true);
    }



    ///////////////////////////////
    //
    // AssistCobrowseListener methods

    @Override
    public void onCobrowseActive() {
        // co-browse active - change the background color to red
        RelativeLayout topLevelLayout = (RelativeLayout) this.findViewById(R.id.root);
        topLevelLayout.setBackgroundColor(Color.RED);
    }

    @Override
    public void onCobrowseInactive() {
        // co-browse inactive - change the background color to white
        RelativeLayout topLevelLayout = (RelativeLayout) this.findViewById(R.id.root);
        topLevelLayout.setBackgroundColor(Color.WHITE);
    }
}
